---
ID: "keiran"
Title: "Keiran Dawson"
Aliases: ["Elricos"]
Categories: ["member"]

Weight: -6

Tagline: "Artist and programmer frequently found laughing, playing board games and eating good food."
Twitter: "Elricos94"

---

Keiran Dawson joined Caveware Digital in January 2015. He enjoys music, eating ramen, and playing games.

Keiran is able to use the LÖVE game engine and can fluently write in Java. He has also written Lua, C++, Python, HTML, CSS, and Javascript.