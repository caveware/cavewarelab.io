---
ID: "chris"
Title: "Chris Alderton"
Aliases: ["Chia"]
Categories: ["member"]

Weight: -4

Twitter: "ChrisAlderton2"
Tagline: "Co-founder, creative game designer and musician with a somewhat sardonic Twitter presence."

---
