---
ID: "bradley"
Title: "Bradley Roope"
Aliases: ["Micaldom"]
Categories: ["member"]

Weight: -6

Twitter: "micaldom"
Tagline: "Developer and designer with a strong interest in digitising his table-top game ideas."

---
