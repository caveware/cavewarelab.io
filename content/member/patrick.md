---
ID: "patrick"
Title: "Patrick Williams"
Aliases: ["Kalacha"]
Categories: ["member"]

Weight: -4

Twitter: "kalacha5016"
Tagline: "Inspired voice actor with the ability to refine game design through deconstruction and discussion."

---
