---
ID: "josef"
Title: "Josef Frank"
Aliases: ["Jojo", "funkeh"]
Categories: ["member"]
Tagline: "Co-founder, lead developer and music producer."
Weight: -19

Twitter: "ohiogauze"

---

A founding member of Caveware Digital and previous director of IGDA Newcastle. He enjoys music, rock climbing and beanies.

Josef is comfortable with the Godot, LÖVE, and Game Maker game engines, and can fluently write Lua, GDScript, and GML code. He has also written C++, CSS, HTML, JavaScript, and Python code.
