---
ID: "georgia"
Title: "Georgia Wilkins"
Aliases: ["Aetherisk"]
Categories: ["member"]

Weight: -3

Twitter: "Aetherisk"
Tagline: "Programmer with a history of divining refined game ideas from complicated premises."

---
