---
ID: "ghost-dance"
Date: 2018-12-17
Title: "Ghost Dance"
Engine: "Godot"
EngineVersion: "3.0"
JamType: "Scream Season"
JamIteration: "2018"
JamLink: "https://screamcatalogue.itch.io/ghost-dance"
Tags: []
itchID: "344597"
ShotCount: 4
Categories: ["game"]
Tagline: "A surreal horror adventure about David Lynch and his work."

Staff: [
  ["josef", "Artist", "Designer", "Musician", "Programmer"],
  ["ryan", "Artist", "Designer", "Musician", "Writer"],
]

---

<i>"The concept of absurdity is something I'm very attracted to." 

~ David Lynch</i>

David Lynch, filmmaker, artist, and musician, has not made a video game. He certainly did not make this video game. If he were to make one, however, maybe it would be something like this complete conceptual and artistic mess. 

In this game you traverse a series of horror scenes directly indebted to Lynch films and artworks cobbled together in an incoherent fever dream. In this world, 'Lynch' is struggling to rebuild his creative world / abstract psychological state and he needs your help.  Only you can traverse the inane dreamscape of Lynch's creations and make some sense of the terrible things that are about to be unleashed. But beware: not all is as it seems.

This game contains lots of tense horror scenes and loud jump scares.

