---
ID: "echo-ducky"
Date: 2017-01-22
Title: "Echo: Ducky"
Engine: "LÖVE"
EngineVersion: "0.10.2"
JamType: "Global Game Jam"
JamIteration: "2017"
JamLink: "http://globalgamejam.org/2017/games/earth-wave"
ShotCount: 1
Tags: []
Categories: ["game"]
Tagline: "A blind protagonist infiltrates a high-security complex using only the sounds reflected off the environment as a guide."
Thanks: ["Liam Crow", "Programmer"]

Staff: [
  ["chris", "Programmer"],
  ["keiran", "Programmer"]
]

---

A blind protagonist infiltrates a high-security complex using only the sounds reflected off the environment as a guide.

Their goal: retrieve the prized rubber duckies to complete their collection. The game includes a menu screen with instructions.

Unfortunately, the physics system has a few hiccups. This breaks the game. You can download it and make some pretty colours at least.