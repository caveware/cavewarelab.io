---
ID: "morkov"
Date: 2016-04-20
Title: "MorkoV"
Engine: "LÖVE"
EngineVersion: "0.10.1"
JamType: "Ludum Dare"
JamIteration: "35"
JamLink: "http://ludumdare.com/compo/ludum-dare-35/?action=preview&uid=29857"
Categories: ["game"]
ShotCount: 3
Tagline: "A game in which carrots are harvested through artificial selection for a despotism in a frigid climate."

Staff: [
  ["josef", "Solo Project"]
]

---

*Morkov* is New Russian for *carrot*.

In this game you must, through selective breeding, cultivate carrots that can withstand the tundra and create a national blue carrot that will boost morale. Only through picking and selling hundreds of carrots can you hope to restore the faded glory that is New Russia.