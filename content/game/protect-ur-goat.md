---
ID: "protect-ur-goat"
Date: 2017-02-20
Title: "protect ur goat"
Engine: "LÖVE"
EngineVersion: "0.10.2"
JamType: "LÖVE Jam"
JamIteration: "2017"
JamLink: "https://funkeh.itch.io/protect-ur-goat"
itchID: "121304"
Categories: ["game"]
ShotCount: 0
Tagline: "An experience in goat protection and incredibly fast game production."

Staff: [
  ["josef", "Musician", "Programmer"],
  ["keiran", "Artist", "Programmer"]
]

---

An experience in goat protection and incredibly fast game production.