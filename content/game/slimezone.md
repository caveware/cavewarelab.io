---
ID: "slimezone"
Date: 2018-10-23
Title: "Slimezone"
Engine: "Twine"
EngineVersion: "idk"
JamType: "Scream Jam"
JamIteration: "2018"
JamLink: "https://screamcatalogue.itch.io/slimezone"
PlayNowLink: "https://screamcatalogue.itch.io/slimezone"
Tags: []
itchID: "320529"
Categories: ["game"]
Tagline: "Suspend your disbelief for a moment and consider a dimension in which logic and convention have no lease, where daily events defy reason, where reality bounds the zone of unreality."

Staff: [
  ["ryan", "Solo Project"]
]

---

Suspend your disbelief for a moment and consider a dimension in which logic and convention have no lease, where daily events defy reason, where reality bounds the zone of unreality. Dear reader, this is a place not for the faint of heart - a place that is about to become seriously messy - and not even you can prevent major slime from going down. 

Dare you enter...

The SLIMEZONE?

*Slimezone* is an interactive fiction story made using Twine that parodies the tone and style of the 'Give Yourself Goosebumps' children's book series.  Approximately 5000 words. Great adventures, great horrors, and great slimes await.  Created in a rushed mania for SCREAM JAM 2018.

