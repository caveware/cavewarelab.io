---
ID: "you-can-shave-the-baby"
Date: 2015-04-21
Title: "You Can Shave The Baby"
Engine: "LÖVE"
EngineVersion: "0.9.2"
JamType: "Ludum Dare"
JamIteration: "32"
JamLink: "http://ludumdare.com/compo/ludum-dare-32/?action=preview&uid=29857"
ShotCount: 3
Tags: []
Categories: ["game"]
Tagline: "Bizarre minigame mayhem in which you can, amongst other things, shave the baby."

Staff: [
  ["cohen", "Hand Model"],
  ["josef", "Designer", "Musician", "Programmer"],
  ["patrick", "Voice Actor"],
  ["ryan", "Artist", "Writer"]
]

---

A series of minigames created for the Ludum Dare.

From downloading the Pope, to covering Colin Mochrie in butter in the bathtub, and, of course - shaving the baby - there is no end to the absurdity.