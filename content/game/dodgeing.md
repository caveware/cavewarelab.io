---
ID: "dodgeing"
Date: 2014-12-08
Title: "Dodgeing"
Engine: "LÖVE"
EngineVersion: "0.9.1"
JamType: "Ludum Dare"
JamIteration: "31"
JamLink: "http://ludumdare.com/compo/ludum-dare-31/?action=preview&uid=31802"
ShotCount: 3
Tags: []
Categories: ["game"]
Tagline: "Ascend platforms and avoid enemies."

Staff: [
  ["bradley", "Solo Project"]
]

---

Ascend platforms and avoid enemies. Sometimes you get blown off screen, and sometimes it crashes.