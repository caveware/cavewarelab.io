---
ID: "broken-down"
Date: 2014-01-26
Title: "Broken Down"
Engine: "LÖVE"
EngineVersion: "0.9.0"
JamType: "Global Game Jam"
JamIteration: "2014"
JamLink: "http://globalgamejam.org/2014/games/broken-down"
ShotCount: 3
Tags: []
Categories: ["game"]
Tagline: "An alien baby and patriarch journey across surreal wastes after their vehicle breaks down."
itchID: "297645"

Staff: [
  ["josef", "Musician", "Programmer"],
  ["mathew", "Artist", "Designer", "Voice Actor"]
]

---

A platformer made for the Global Game Jam in which two aliens must guide one another across landscapes dotted with portals, tunnels, switches, and traps. In order to progress, the player must switch between father and son to access inaccessible areas.