---
ID: "earth-wave"
Date: 2017-01-22
Title: "Earth Wave"
Engine: "Unity"
EngineVersion: "5.4"
JamType: "Global Game Jam"
JamIteration: "2017"
JamLink: "http://globalgamejam.org/2017/games/earth-wave"
ShotCount: 3
Tags: []
Categories: ["game"]
Tagline: "You control the landscape, not the player."

Staff: [
  ["cohen", "Musician", "Programmer"],
  ["georgia", "Programmer"],
  ["mathew", "Artist", "Designer"],
  ["robert", "Artist"]
]

---

Change the amplitude and frequency of the wave to avoid obstacles and collect energy to keep running.
