---
ID: "the-great-migration"
Date: 2016-04-20
Title: "The Great Migration"
Engine: "LÖVE"
EngineVersion: "0.10.1"
JamType: "Ludum Dare"
JamIteration: "35"
JamLink: "http://ludumdare.com/compo/ludum-dare-35/?action=preview&uid=31802"
Categories: ["game"]
ShotCount: 3
Tagline: "A Flappy Bird clone with the twist that you have to alternate screens, plunging both under the water and soaring above the land."

Staff: [
  ["bradley", "Solo Project"]
]

---

A Flappy Bird clone with the twist that you have to alternate screens, plunging both under the water and soaring above the land.