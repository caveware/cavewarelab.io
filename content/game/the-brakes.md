---
ID: "the-brakes"
Date: 2015-12-15
Title: "THE BRAKES"
Engine: "LÖVE"
EngineVersion: "0.9.2"
JamType: "Ludum Dare"
JamIteration: "34"
JamLink: "http://ludumdare.com/compo/ludum-dare-34/?action=preview&uid=29857"
Categories: ["game"]
ShotCount: 3
Tagline: "A geometric puzzle game where starfields must be crossed and enemies duly avoided."

Staff: [
  ["josef", "Artist", "Musician", "Programmer"],
  ["patrick", "Voice Actor"]
]

---

A geometric puzzle game where starfields must be crossed and enemies duly avoided.