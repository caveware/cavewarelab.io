---
ID: "oops-you-joined-a-death-cult"
Date: 2016-01-31
Title: "Oops! You Joined A Death Cult"
Engine: "Twine"
EngineVersion: "2.0.10"
JamType: "Global Game Jam"
JamIteration: "2016"
JamLink: "http://globalgamejam.org/2016/games/choose-your-own-adventure-oops-you-joined-death-cult"
Categories: ["game"]
ShotCount: 3
Tagline: "A choose-your-own text adventure in which a pugilistic child grapples with a narrator that is attempting to kill him off."

Staff: [
  ["ryan", "Solo Project"]
]

---

A text adventure in the style of a choose-your-own-adventure text created for the Global Game Jam 2016. You must persevere against an endless cycle of deaths and bad choices in order to uncover the truth behind a strange series of events.