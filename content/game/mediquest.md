---
ID: "mediquest"
Date: 2013-12-16
Title: "MediQuest"
Engine: "LÖVE"
EngineVersion: "0.9.0"
JamType: "Ludum Dare"
JamIteration: "28"
JamLink: "http://www.ludumdare.com/compo/ludum-dare-28/comment-page-2/?action=preview&uid=29857"
Categories: ["game"]
itchID: "297148"
ShotCount: 3
Tagline: "Fantasy platformer with only a single respawn point as a lifeline."

Staff: [
  ["chris", "Programmer"],
  ["cohen", "Musician"],
  ["josef", "Musician", "Programmer"],
  ["mathew", "Artist", "Designer"]
]

---

Your poor grandma is sick and she needs you to retrieve a very strong medicine for her.

You must go on a quest to find it and bring it back. She has given you a teleportation totem and stone, so that if you fall to danger, you will be resurrected. But you can only place the resurrection point once.