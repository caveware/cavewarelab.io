---
ID: "samsara-interactive"
Date: 2015-01-25
Title: "Samsara Interactive"
Engine: "LÖVE"
EngineVersion: "0.9.1"
JamType: "Global Game Jam"
JamIteration: "2015"
JamLink: "http://globalgamejam.org/2015/games/samsara-interactive"
Categories: ["game"]
ShotCount: 3
Tagline: "An oblong being navigates a apocalyptic cosmological hellscape in attempt to restore balance to the universe."

Staff: [
  ["josef", "Designer", "Musician", "Programmer"],
  ["ryan", "Artist", "Designer", "Musician", "Writer"]
]

---

A surrealist adventure game inspired by mythology created for the Global Game Jam. The player is tasked with restoring balance to the world and traverses a bizarre series of rooms searching for enlightenment. Many of these are disturbing.