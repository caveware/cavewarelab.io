---
ID: "sudoku-adventure"
Date: 2015-11-24
Title: "Sudoku Adventure"
Engine: "LÖVE"
EngineVersion: "0.9.2"
JamType: "MiniLD"
JamIteration: "63"
JamLink: "http://ludumdare.com/compo/minild-63/?action=preview&uid=31802"
Categories: ["game"]
ShotCount: 3
Tagline: "Sudoku-based puzzle-platformer."

Staff: [
  ["bradley", "Solo Project"]
]

---

An unfinished game that alternates between completing a sudoku puzzle and finding the correct numbers through traversing a platformer maze.