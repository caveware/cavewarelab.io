---
ID: "cooking-llama"
Date: 2015-06-07
Title: "Cooking Llama"
Engine: "LÖVE"
EngineVersion: "0.9.2"
JamType: "Hectic Games Jam"
JamIteration: "6"
JamLink: "https://www.facebook.com/events/1433149153656857/"
ShotCount: 3
Tags: []
Categories: ["game"]
Tagline: "A llama without opposable thumbs is tasked with creating bland food in a discernibly Orient locale."
Thanks: [
  ["Shakari Chulyin", "Artist"]
]

Staff: [
  ["josef", "Programmer"],
  ["patrick", "Designer", "Voice Actor"],
  ["ryan", "Musician", "Writer"]
]

---

An unfinished cooking game for the Hectic Games Jam. Consisting of trial-and-error recipes and minigames, the original concept was that you would need to respond to cues and symbols in order to learn recipes.