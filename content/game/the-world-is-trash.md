---
ID: "the-world-is-trash"
Date: 2019-01-27
Title: "the world is trash"
Engine: "Godot"
EngineVersion: "3.1"
JamType: "Global Game Jam"
JamIteration: "2019"
JamLink: "https://globalgamejam.org/2019/games/world-trash"
Tags: []
itchID: "361233"
ShotCount: 3
Categories: ["game"]
Tagline: "A soothing experience about cleaning up the mess humans make."

Staff: [
  ["josef", "Artist", "Designer", "Musician", "Programmer"],
  ["ryan", "Artist", "Designer", "Musician", "Writer"],
]

---

one day, mankind colonized our planet, mars. it was kind of rude of them to do that and i'm glad they left. however they left all this trash behind :(

we're going to have to clean up all the trash. fortunately you can help us with your cannon - just click to shoot us into the trash and we'll dispose of it properly! 

while you're here you can learn about the planet mars, its features and sights, and explore the different areas that our world has to offer :) 

wasd to move and click to shoot. recycle responsibly!
