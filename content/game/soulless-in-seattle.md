---
ID: "soulless-in-seattle"
Date: 2018-10-24
Title: "Soulless In Seattle"
Engine: "Godot"
EngineVersion: "3.0"
JamType: "Scream Jam"
JamIteration: "2018"
JamLink: "https://funkeh.itch.io/soulless-in-seattle"
PlayNowLink: "https://funkeh.itch.io/soulless-in-seattle"
Tags: []
itchID: "320218"
ShotCount: 3
Categories: ["game"]
Tagline: "A newly adorned culinary ghost, you have one task - to find the least spookable animal in a quiet Seattle household."

Staff: [
  ["josef", "Solo Project"]
]

---

A candle flickers.

The moonlight glimmers.

A mystery is afoot.

A newly adorned culinary ghost, you have one task - to find the least spookable animal in a quiet Seattle household.

