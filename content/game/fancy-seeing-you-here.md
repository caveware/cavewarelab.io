---
ID: "fancy-seeing-you-here"
Date: 2017-04-25
Title: "Fancy Seeing You Here"
Engine: "LÖVE"
EngineVersion: "0.10.2"
JamType: "Ludum Dare"
JamIteration: "38"
JamLink: "https://ldjam.com/events/ludum-dare/38/fancy-seeing-you-here"
Tags: []
ShotCount: 1
Categories: ["game"]
Tagline: "The party is never as fun as the afterparty."

Staff: [
  ["keiran", "Artist", "Musician", "Programmer"],
  ["josef", "Artist", "Musician", "Programmer", "Writer"],
  ["mathew", "Artist", "Vocalist"],
  ["robert", "Artist", "Programmer"]
]

---

A woman goes to a party and her life changes forever.