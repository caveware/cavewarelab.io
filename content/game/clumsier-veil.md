---
ID: "clumsier-veil"
Date: 2018-01-28
Title: "CLUMSIER_VEIL"
Engine: "Phaser"
EngineVersion: "2.10.0"
JamType: "Global Game Jam"
JamIteration: "2018"
JamLink: "https://globalgamejam.org/2018/games/clumsierveil"
PlayNowLink: "https://cv.caveware.digital"
Tags: []
ShotCount: 2
Categories: ["game"]
Tagline: "Working to protect their utopian homeland from dangerous rebels, a government official deciphers enemy messages."

Staff: [
  ["georgia", "Artist", "Mathematician", "Programmer"],
  ["josef", "Artist", "Musician", "Programmer"]
]

---

Working to protect their utopian homeland from dangerous rebels, a government official works to decipher enemy messages.

