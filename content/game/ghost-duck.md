---
ID: "ghost-duck"
Date: 2015-08-25
Title: "Ghost Duck"
Engine: "LÖVE"
EngineVersion: "0.9.2"
JamType: "Ludum Dare"
JamIteration: "33"
JamLink: "http://ludumdare.com/compo/ludum-dare-33/?action=preview&uid=29857"
ShotCount: 3
Tags: []
Categories: ["game"]
Tagline: "A mystery adventure in which a malevolent duck disposes of the victims of a haunted house."

Staff: [
  ["josef", "Musician", "Programmer"],
  ["mathew", "Artist", "Designer"],
  ["patrick", "Designer"],
  ["ryan", "Artist", "Writer"]
]

---

A horror adventure game inspired by Maniac Mansion. In *Ghost Duck*, the player doesn't get to play as a group of college girls investigating the old Henderson mansion. They do, however, play as a duck.

This duck is a Mallard duck that embodies the spirits of the late Henderson family. His little wings and webbed feet may not be seen by mortals, but his presence can certainly be felt.

And tonight, on the night of the murder that sealed the Henderson curse, death comes once again to this mansion...