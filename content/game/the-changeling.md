---
ID: "the-changeling"
Date: 2017-01-22
Title: "The Changeling"
Engine: "LÖVE"
EngineVersion: "0.10.2"
JamType: "Global Game Jam"
JamIteration: "2017"
JamLink: "http://globalgamejam.org/2017/games/changeling"
Categories: ["game"]
ShotCount: 3
Tagline: "Their heartbeat may be stopped, but their brainwaves are at your command."

Staff: [
  ["josef", "Musician", "Programmer"],
  ["ryan", "Artist", "Musician", "Writer"]
]

---

A comedic adventure game in which you are a parasitic being that can infiltrate the brains of other life forms and control their bodies.

Set free by an imprisoned neuroscientist who plans to unleash you upon the world, you must venture from host to host as you find a way to rescue your creator from a life behind bars. Or you could just eat his brain. The game's central mechanic is using the parasite to find and control different characters in the game world.

Depending on which form you take, other characters will respond to you differently. Some prisoners may share secrets with you. Others may swear at you. This happens a lot. Either way, there is much brain-defiling excitement to be had.

Definitely not for babies.