---
ID: "scarnon-room-escape"
Date: 2015-01-25
Title: "Scarnon Room Escape"
Engine: "LÖVE"
EngineVersion: "0.9.1"
JamType: "Global Game Jam"
JamIteration: "2015"
JamLink: "http://globalgamejam.org/2015/games/scarnon-room-escape"
Categories: ["game"]
ShotCount: 3
Tagline: "A girl commits property damage of escalating severity in order to escape from a series of cavernous rooms."

Staff: [
  ["chris", "Musician", "Programmer"],
  ["keiran", "Programmer"],
  ["mathew", "Artist", "Designer"]
]

---

A room escape game in which players utilise and combine found items by investigating and destroying cabinets, chests, and lamps in order to get out. Whoever owns this place is probably going to be pretty mad you broke all those locks, so you better leave fast!