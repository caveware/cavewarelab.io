---
ID: "asteroids"
Date: 2017-04-15
Title: "Asteroids"
Engine: "LÖVE"
EngineVersion: "0.10.2"
Tags: []
Categories: ["game"]
Tagline: "A relaxed reimplementation of the classic."

Staff: [
  ["josef", "Artist", "Musician", "Programmer"],
  ["robert", "Artist", "Programmer"]
]

---

A relaxed reimplementation of the classic.