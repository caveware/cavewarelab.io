---
ID: "aketa"
Title: "Aketa"
Date: 2014-12-08
Engine: "LÖVE"
EngineVersion: "0.9.1"
JamType: "Ludum Dare"
JamIteration: "31"
JamLink: "http://ludumdare.com/compo/ludum-dare-31/?action=preview&uid=29857"
ShotCount: 3
Tags: []
Categories: ["game"]
Tagline: "In an ever-changing construct, a girl must navigate labyrinthine passages to find her escape."

Staff: [
  ["cohen", "Musician"],
  ["keiran", "Artist"],
  ["josef", "Musician", "Programmer"],
  ["mathew", "Artist", "Designer"]
]

---

A platformer that takes place on one stage that consistently transforms as you traverse spike traps, lava pits and moving platforms in an attempt to find a way out.