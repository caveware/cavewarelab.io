---
ID: "star-turtle-64"
Date: 2014-08-26
Title: "Star Turtle 64"
Engine: "LÖVE"
EngineVersion: "0.9.1"
JamType: "Ludum Dare"
JamIteration: "30"
JamLink: "http://www.ludumdare.com/compo/ludum-dare-30/?action=preview&uid=29857"
Categories: ["game"]
itchID: "329954"
ShotCount: 3
Tagline: "Galactic turtle action on hostile planets."

Staff: [
  ["bradley", "Artist", "Programmer"],
  ["chris", "Programmer"],
  ["cohen", "Musician"],
  ["georgia", "Android Developer"],
  ["josef", "Musician", "Programmer"],
  ["mark", "Artist", "Programmer"],
  ["mathew", "Artist", "Designer"],
  ["ryan", "Musician", "Writer"]
]

---

A shoot-em-up game concieved in a separate universe to its predecessor, *Turtle Simulator*.

You are the last turtle. The spirit of the Elder Turtle stirs you to search for your brethren. You must rejoin your kind and become one with the Elder Turtle. You must search the ends of the galaxy to unite with your turtle kind.
