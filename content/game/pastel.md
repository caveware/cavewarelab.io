---
ID: "pastel"
Date: 2014-01-13
Title: "PASTEL"
Engine: "LÖVE"
EngineVersion: "0.9.0"
JamType: "MiniLD"
JamIteration: "48"
JamLink: "http://www.ludumdare.com/compo/minild-48/comment-page-1/?action=preview&uid=32779"
Categories: ["game"]
itchID: "297623"
ShotCount: 3
Tagline: "Surmount pixel platforms in faded pastel hues."

Staff: [
  ["josef", "Solo Project"]
]

---

A platformer made for the MiniLD with a colorful twist: the game changes based upon the hue of the platform the player is on, changing the color and structure of the level.