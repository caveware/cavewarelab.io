---
ID: "turtle-simulator"
Date: 2014-04-29
Title: "Turtle Simulator"
Engine: "LÖVE"
EngineVersion: "0.9.1"
JamType: "Ludum Dare"
JamIteration: "29"
JamLink: "http://www.ludumdare.com/compo/ludum-dare-29/?action=preview&uid=29857"
ShotCount: 3
Tags: []
itchID: "298289"
Categories: ["game"]
Tagline: "Marine RPG adventure in which a baby turtle conquers the seas with its flippers and faces unexpected challenges."
Thanks: [
  ["Brianna Fazio", "Artist"]
]

Staff: [
  ["josef", "Programmer"],
  ["mark", "Artist", "Programmer"],
  ["mathew", "Artist", "Designer"],
  ["ryan", "Artist", "Designer", "Musician", "Writer"]
]

---

An RPG developed for the Ludum Dare.

It features a baby turtle, who, separated from its mother, must learn to swim and venture the seas in search of its matriarch. On its way, the little turtle encounters profound truisms about life and death in the food chain, and is ultimately faced with a great choicee.

Featuring turtle-slapping combat, contemporary abstract art, and, on the topic of art, also the singer-songwriter Art Garfunkel.