---
ID: "yogo-rails"
Date: 2013-12-16
Title: "YOGO Rails"
Engine: "Game Maker"
EngineVersion: "8.1"
JamType: "Ludum Dare"
JamIteration: "28"
JamLink: "http://www.ludumdare.com/compo/ludum-dare-28/?action=preview&uid=31802"
ShotCount: 3
Tags: []
itchID: "297442"
Categories: ["game"]
Tagline: "A game in which you are constantly moving on rails and must move around a maze of tracks with switches to reach the goal."

Staff: [
  ["bradley", "Solo Project"]
]

---

A game in which you are constantly moving on rails and must move around a maze of tracks with switches to reach the goal. This one is quite buggy.