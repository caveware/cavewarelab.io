---
ID: "the-grand-salad"
Date: 2016-01-31
Title: "The Grand Salad"
Engine: "LÖVE"
EngineVersion: "0.10.0"
JamType: "Global Game Jam"
JamIteration: "2016"
JamLink: "http://globalgamejam.org/2016/games/grand-salad"
Categories: ["game"]
ShotCount: 3
Tagline: "An RPG adventure set in an edible world of leafy greens and condiments."
Thanks: [
  ["Aemyn Connolly", "Musician"]
]

Staff: [
  ["josef", "Programmer"],
  ["keiran", "Artist", "Programmer"],
  ["mathew", "Artist", "Designer"]
]

---

An RPG adventure set in an edible world of leafy greens and condiments.