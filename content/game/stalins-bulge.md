---
ID: "stalins-bulge"
Date: 2015-01-25
Title: "Stalin's Bulge"
Engine: "LÖVE"
EngineVersion: "0.9.1"
JamType: "Global Game Jam"
JamIteration: "2015"
JamLink: "http://globalgamejam.org/2015/games/stalins-bulge"
Categories: ["game"]
ShotCount: 3
Crude: true
Tagline: "Edgy beat-em-up featuring a mass-murdering moustachioed maniac."

Staff: [
  ["bradley", "Programmer"],
  ["cohen", "Musician", "Writer"],
  ["mark", "Artist", "Designer", "Writer"]
]

---

There are games that invigorate the mind and enchant the soul, games that inspire young and old alike to develop their own imaginations, and transform the world for the better.

*Stalin's Bulge* is not one of those games.