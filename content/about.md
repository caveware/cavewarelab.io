---
title: "About"
description: "about this blog"
date: "2018-02-11"
Weight: 5
---

## > About

_We are Caveware Digital, a collective of eleven content creators from New South Wales, Australia._

<img style="width: 100%" alt="Most members of Caveware Digital" src="/jpg/banner.jpg" />
<p style="text-align: center;font-size: .7em">_Most of Caveware Digital excluding Mark, Patrick and Bradley, plus including Liam_</p>

We originally formed in June 2011 as Funkeh Chikun Games, a side-project of a high school record label. After many attempts to create a full game, this group dissipated in 2013. Later that year, four members of that group decided to try again under a new name, chikun Games. The original founders of chikun Games were Chris Alderton, Mathew Dwyer, Josef Frank, and Rory Mills. After growing from a small group of budding game developers to a collective of eleven members, chikun Games decided to rename itself to Caveware Digital in January 2017 with a renewed focus on profitable games.

We frequently participate in game jams, including the Ludum Dare and the Global Game Jam. We are also affiliated with the Newcastle, NSW branch of the IGDA.

**ABN**: [27 879 337 469](https://connectonline.asic.gov.au/RegistrySearch/faces/landing/bySearchId.jspx?searchId=621083903&searchIdType=BUSN&_adf.ctrl-state=upcsz15ia_4)

{{< rawhtml >}}
<style>
  #content p {
    font-size: 1em;
  }
</style>
{{< /rawhtml >}}